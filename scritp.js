let alunos = document.querySelector('input#aluno')
let notas = document.querySelector('input#nota')
//let listaPar = document.querySelector('select#alista')
let listaPar = document.querySelector('div#alista')
let res = document.querySelector('div#res')
let ranking = []
//let ranking = [{ nome: 'a', nota: 1 }, { nome: 'b', nota: 10 }, { nome: 'c', nota: 2 }]

document.querySelector('input#aluno').addEventListener('keydown', function (evento) {
    //alert(event.keyCode)
    if (evento.keyCode == 13) {
        notas.focus()
    }
})

document.querySelector('input#nota').addEventListener('keydown', function (evento) {
    //alert(event.keyCode)
    if (evento.keyCode == 13) {
        gravar()
    }
})

function gravar() {
    let valorAlunos = alunos.value
    let valorNotas = notas.value
    if (valorAlunos.length == 0 || valorNotas.length == 0) {
        window.alert('Por favor preencha todos os campos')
    } else {
        ranking.push({ nome: valorAlunos, nota: Number(valorNotas) })

        for (let pos in ranking) {
            listaPar.innerHTML = ''
            let parcial = document.createElement('p')
            parcial.innerHTML = `A nota do aluno <strong>${ranking[pos].nome}</strong> foi gravada com sucesso!`
            listaPar.appendChild(parcial)
        }
    }
    notas.value = ''
    alunos.value = ''
    alunos.focus()
}

function exibir() {
    res.innerHTML = ''
    if (ranking.length == 0) {
        window.alert('Insira os dados de pelo menos 1 aluno!')
        res.innerHTML = `<p>Olá...</p>`
    } else {
        ranking.sort(function (a, b) {
            return b.nota - a.nota
        })
        //ranking.reverse()
        for (let pos in ranking) {
            //console.log(`O Aluno ${ranking[pos].nome} ficou em ${Number(pos) + 1}º lugar com a nota ${ranking[pos].nota}`)
            let item = document.createElement('p')
            item.innerHTML = `O Aluno <strong>${ranking[pos].nome}</strong> ficou em <strong>${Number(pos) + 1}º</strong> lugar com a nota <strong>${ranking[pos].nota}</strong>`
            res.appendChild(item)

        }
    }
}

function limpar() {
    ranking = []
    listaPar.innerHTML = `<p>Tudo Limpo!</p>`
    res.innerHTML = `<p>Tudo Limpo!</p>`
}

